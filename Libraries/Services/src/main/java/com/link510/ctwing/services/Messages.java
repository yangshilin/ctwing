package com.link510.ctwing.services;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.link510.ctwing.core.domain.device.DeviceInfo;
import com.link510.ctwing.core.domain.message.*;
import com.link510.ctwing.core.enums.message.MsgLevel;
import com.link510.ctwing.core.helper.StringHelper;
import com.link510.ctwing.core.helper.UnixTimeHelper;
import com.link510.ctwing.core.locks.MessageLocker;
import com.link510.ctwing.data.MessageImages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by cqnews on 2017/4/11.
 */

//消息
@Service(value = "Messages")
public class Messages {


    @Resource(name = "MessagesData")
    private com.link510.ctwing.data.Messages messages;

    @Resource(name = "HeartMessagesData")
    private com.link510.ctwing.data.HeartMessages heartMessages;


    @Resource(name = "MessageRecordsData")
    private com.link510.ctwing.data.MessageRecords messageRecords;


    @Autowired
    private MessageLocker messageLocker;

    @Autowired
    private Logs logs;

    @Autowired
    private Devices devices;

    @Autowired
    private MessageImages messageImages;


    //region  消息方法

    /**
     * 获得消息数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getMessageCount(Specification<MessageInfo> condition) {

        try {
            return messages.getMessageCount(condition);
        } catch (Exception ex) {
            logs.write(ex, "获得消息数量失败");
        }
        return 0;
    }

    /**
     * 创建一条消息数据
     *
     * @param messageInfo 消息模型
     * @return 返回创建信息
     **/
    public MessageInfo createMessage(MessageInfo messageInfo) {
        try {
            return messages.createMessage(messageInfo);
        } catch (Exception ex) {
            logs.write(ex, "创建一条消息数据失败");
        }
        return null;
    }

    /**
     * 更新一条消息数据
     *
     * @param messageInfo 消息模型
     **/
    public void updateMessage(MessageInfo messageInfo) {
        try {
            messages.updateMessage(messageInfo);
        } catch (Exception ex) {
            logs.write(ex, "更新一条消息数据异常");
        }

    }

    /**
     * 删除一条消息数据
     *
     * @param id 消息模型
     **/
    public void deleteMessageById(Integer id) {
        try {
            messages.deleteMessageById(id);
        } catch (Exception ex) {
            logs.write(ex, "删除一条消息数据异常");
        }
    }

    /**
     * 批量删除一批消息数据
     **/
    public void deleteMessageByIdList(String idList) {
        try {
            messages.deleteMessageByIdList(idList);
        } catch (Exception ex) {
            logs.write(ex, "批量删除一批消息数据异常");
        }
    }

    /**
     * 获取一条消息数据
     *
     * @param id 消息模型
     **/
    public MessageInfo getMessageById(Integer id) {
        try {
            return messages.getMessageById(id);
        } catch (Exception ex) {
            logs.write(ex, "获取一条消息数据");
        }

        return null;
    }


    /**
     * 获取最后一条数据
     *
     * @param deviceSN 设备编号
     * @param token    token
     * @return
     */
    public MessageInfo getLastMessageByTokenAndDeviceSN(String token, String deviceSN) {
        try {
            return messages.getLastMessageByTokenAndDeviceSN(token, deviceSN);
        } catch (Exception ex) {
            logs.write(ex, "获取最后一条数据");
        }


        return null;
    }

    /**
     * 获取最后一条数据
     *
     * @param deviceSN  设备编号
     * @param protocols 协议名称
     * @return
     */
    public MessageInfo getLastMessageByDeviceSNAndProtocol(String deviceSN, String... protocols) {
        return getLastMessageByDeviceSNAndProtocol(deviceSN, UnixTimeHelper.getUnixTimeStamp(), protocols);
    }

    /**
     * 获取最后一条数据
     *
     * @param deviceSN  设备编号
     * @param protocols 协议名称
     * @return
     */
    public MessageInfo getLastMessageByDeviceSNAndProtocol(String deviceSN, Integer timestamp, String... protocols) {

        try {

            return messages.getLastMessageByDeviceSNAndProtocol(deviceSN, timestamp, protocols);

        } catch (Exception ex) {
            logs.write(ex, "获取一条消息数据");
        }

        return null;
    }


    /**
     * 获得消息数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回MessageInfo
     **/
    public List<MessageInfo> getMessageList(Specification<MessageInfo> condition, Sort sort) {

        List<MessageInfo> messageList = new ArrayList<MessageInfo>();

        try {
            messageList = messages.getMessageList(condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得消息数据列表异常");
            logs.write(ex, "获得消息数据列表异常");
        }

        return messageList;
    }


    /**
     * 获得消息数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回MessageInfo
     **/
    public Page<MessageInfo> getMessageList(Integer pageSize, Integer pageNumber, Specification<MessageInfo> condition, Sort sort) {

        Page<MessageInfo> messageList = null;

        try {
            messageList = messages.getMessageList(pageSize, pageNumber, condition, sort);
        } catch (Exception ex) {

            ex.printStackTrace();

            logs.write(ex, "获得消息数据列表异常");
        }

        return messageList;
    }

    /**
     * 获取列表条件
     *
     * @param msgId    消息id
     * @param token    token
     * @param deviceSN 设备编号
     * @return
     */
    public Specification<MessageInfo> getMessageListCondition(Integer msgId, String token, String deviceSN, String keyword) {
        return getMessageListCondition(msgId, token, deviceSN, keyword, "");
    }


    /**
     * 获取列表条件
     *
     * @param msgId    消息id
     * @param token    token
     * @param deviceSN 设备编号
     * @param keyword  关键词
     * @param uid      uid
     * @return 条件
     */
    public Specification<MessageInfo> getMessageListCondition(Integer msgId, String token, String deviceSN, String keyword, String uid) {
        return (Specification<MessageInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();

            if (msgId >= 1) {

                list.add(cb.equal(root.get("msgId").as(Integer.class), msgId));
            }

            if (StringHelper.isNotNullOrWhiteSpace(token)) {
                list.add(cb.equal(root.get("token").as(String.class), token));
            }

            if (StringHelper.isNotNullOrWhiteSpace(deviceSN)) {
                list.add(cb.equal(root.get("deviceSN").as(String.class), deviceSN));
            }

            if (StringHelper.isNotNullOrWhiteSpace(keyword)) {
                list.add(cb.like(root.get("content").as(String.class), "%" + keyword + "%"));
            }

            if (StringHelper.isNotNullOrWhiteSpace(uid)) {
                list.add(cb.like(root.get("uid").as(String.class), uid));
            }


            Predicate[] p = new Predicate[list.size()];

            query.where(list.toArray(p));

            return query.getGroupRestriction();
        };
    }

    /**
     * 获取代理列表条件
     *
     * @param msgId    消息id
     * @param token    token
     * @param deviceSN 设备编号
     * @param keyword  关键词
     * @param agentId  代理Id
     * @return
     */
    public Specification<MessageInfo> getAegntMessageListCondition(Integer msgId, String token, String deviceSN, String keyword, Integer agentId) {
        return (Specification<MessageInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();

            if (msgId >= 1) {

                list.add(cb.equal(root.get("msgId").as(Integer.class), msgId));
            }

            if (StringHelper.isNotNullOrWhiteSpace(token)) {
                list.add(cb.equal(root.get("token").as(String.class), token));
            }

            if (StringHelper.isNotNullOrWhiteSpace(deviceSN)) {
                list.add(cb.equal(root.get("deviceSN").as(String.class), deviceSN));
            }

            if (StringHelper.isNotNullOrWhiteSpace(keyword)) {
                list.add(cb.like(root.get("content").as(String.class), "%" + keyword + "%"));
            }

            if (agentId >= 1) {
                list.add(cb.equal(root.get("agentId").as(Integer.class), agentId));
            }


            Predicate[] p = new Predicate[list.size()];

            query.where(list.toArray(p));

            return query.getGroupRestriction();
        };
    }


    /**
     * 获取列表条件
     *
     * @return
     */
    public Specification<MessageInfo> getMessageAdminListCondition() {
        return (Specification<MessageInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();


            Predicate[] p = new Predicate[list.size()];

            query.where(list.toArray(p));

            return query.getGroupRestriction();
        };
    }

    /**
     * 更新消息的设备号
     *
     * @param msgInfo  消息
     * @param devicdSN 设备号
     * @param protocl  协议名
     */
    @Async
    public void updateMessageDeviceInfo(MessageInfo msgInfo, String devicdSN, String protocl, Integer msgLevel, String protodesc) {

        try {

            DeviceInfo deviceInfo = devices.getDeviceByDeviceSN(devicdSN);

            if (deviceInfo == null || deviceInfo.getDeviceId() <= 0) {
                return;
            }


            msgInfo.setUid(deviceInfo.getUid());
            msgInfo.setDeviceSN(deviceInfo.getDeviceSN());
            msgInfo.setUnitCode(deviceInfo.getUnitCode());
            msgInfo.setProductType(deviceInfo.getProductType());
            msgInfo.setProtocol(protocl);
            msgInfo.setMsgLevel(msgLevel);
            msgInfo.setProtodesc(protodesc);
            updateMessage(msgInfo);

        } catch (Exception ex) {
            logs.write(ex, "更新消息的设备号");
        }


    }

    /**
     * 统计在指定的时间内的消息数目
     *
     * @param deviceSN     设备id
     * @param productId    产品Id
     * @param protocol     协议
     * @param intervalTime 时间段
     * @return
     */
    public long countIntervalMessage(String deviceSN, Integer productId, String protocol, Integer intervalTime) {

        try {
            return messages.countIntervalMessage(deviceSN, productId, protocol, intervalTime);
        } catch (Exception ex) {
            logs.write(ex, "统计在指定的时间内的消息数目");
        }
        return 0;
    }


    /**
     * 更新消息部分的设备编号,协议和token
     *
     * @param msgInfo  消息
     * @param devicdSN 设备编号
     * @param protocl  协议
     * @param msgLevel
     * @param token    token
     */
    @Async
    public void updateMessageDeviceInfoAndToken(MessageInfo msgInfo, String devicdSN, String protocl, Integer msgLevel, String token) {

        try {

            DeviceInfo deviceInfo = devices.getDeviceByDeviceSN(devicdSN);

            if (deviceInfo == null || deviceInfo.getDeviceId() <= 0) {
                return;
            }


            msgInfo.setDeviceSN(deviceInfo.getDeviceSN());
            msgInfo.setAgentId(deviceInfo.getAgentId());
            msgInfo.setUnitCode(deviceInfo.getUnitCode());
            msgInfo.setProductType(deviceInfo.getProductType());
            msgInfo.setProtocol(protocl);
            msgInfo.setMsgLevel(msgLevel);
            msgInfo.setMessage(msgInfo.getMessage() + ",消息来自" + token);
            msgInfo.setToken(token);
            updateMessage(msgInfo);

        } catch (Exception ex) {
            logs.write(ex, "更新消息部分的设备编号,协议和token");
        }
    }

    /**
     * 统计token的心跳次数
     *
     * @param token        网关
     * @param type         消息类型
     * @param intervalTime 时间段
     */
    public long countTokenIntervalMessage(String token, Integer type, Integer intervalTime) {
        try {
            return messages.countTokenIntervalMessage(token, type, intervalTime);
        } catch (Exception ex) {
            logs.write(ex, "统计在指定的时间内的消息数目");
        }
        return 0;
    }


    /**
     * 统计在指定的时间内的消息数目
     *
     * @param msgId
     * @param deviceSN     设备编号
     * @param intervalTime 时间段
     * @return
     */
    public long countIntervalMessage(Integer msgId, Integer productId, String deviceSN, String protocol, Integer intervalTime) {

        messageLocker.defaultLock.lock();

        try {

            return messages.countIntervalMessage(msgId, productId, deviceSN, protocol, intervalTime);

        } catch (Exception ex) {

            logs.write(ex, "统计在指定的时间内的消息数目");

        } finally {

            messageLocker.defaultLock.unlock();

        }
        return 0;
    }


    /**
     * 统计警告设备消息
     *
     * @return long
     */
    public long countWarningAllMessage() {

        try {

            return messages.countWarningAllMessage();

        } catch (Exception ex) {
            logs.write(ex, "统计警告设备消息");
        }

        return 0;
    }

    /**
     * 让消息失效
     *
     * @param msgId 消息id
     */
    public void lose(Integer msgId) {

        try {
            messages.lose(msgId);
        } catch (Exception ex) {
            logs.write(ex, "消息失效");
        }

    }

    /**
     * 通过tokem查询消息列表
     *
     * @param token
     * @return
     */
    public List<MessageInfo> getMessageByToken(String token) {

        List<MessageInfo> messageInfoList = new ArrayList<>();

        try {

            messageInfoList = messages.getMessageByToken(token);

        } catch (Exception ex) {
            logs.write(ex, "通过tokem查询消息列表");
        }

        return messageInfoList;

    }


    //endregion

    //region  消息解析记录方法

    /**
     * 获得消息解析记录数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getMessageRecordCount(Specification<MessageRecordInfo> condition) {

        try {
            return messageRecords.getMessageRecordCount(condition);
        } catch (Exception ex) {
            logs.write(ex, "获得消息解析记录数量失败");
        }
        return 0;
    }


    /**
     * 创建一条消息解析记录数据
     *
     * @param recordInfo 消息解析记录模型
     * @return 返回创建信息
     **/
    private void createMessageRecord(MessageRecordInfo recordInfo) {
        try {
            messageRecords.createMessageRecord(recordInfo);
        } catch (Exception ex) {
            logs.write(ex, "创建一条消息解析记录数据失败");
        }
    }


    /**
     * 创建消息
     *
     * @param messageInfo 消息
     * @param recordList        解析参数
     */
    public boolean createMessageAndRecordList(MessageInfo messageInfo, List<ProtocolRecord> recordList) {
        try {

            if (messageInfo == null) {
                throw new IllegalArgumentException("消息不能为空");
            }

            messageInfo = createMessage(messageInfo);

            if (messageInfo == null || messageInfo.getMsgId() <= 0) {
                throw new IOException("数据存储失败");
            }

            if (recordList != null && recordList.size() >= 1) {


                createMessageRecord(messageInfo.getMsgId(),
                        messageInfo.getToken(),
                        messageInfo.getDeviceSN(),
                        messageInfo.getProtocol(),
                        messageInfo.getMsgLevel(),
                        messageInfo.getProtodesc(),
                        recordList);


                return false;
            }


        } catch (Exception ex) {
            logs.write(ex, "创建消息");
        }
        return false;
    }


    /**
     * 创建解析记录
     *
     * @param msgId      消息Id
     * @param token      token
     * @param deviceSN   设备编号
     * @param protocl    协议
     * @param protodesc  协议描述
     * @param recordList 解析记录
     */
    public void createMessageRecord(Integer msgId, String token, String deviceSN, String protocl, Integer msgLevel, String protodesc, List<ProtocolRecord> recordList) {

        Integer timestamp = UnixTimeHelper.getUnixTimeStamp();

        for (ProtocolRecord record : recordList) {

            String value = record.getValue();


            if (StringHelper.isNullOrWhiteSpace(value)) {
                continue;
            }

            MessageRecordInfo recordInfo = new MessageRecordInfo();

            recordInfo.setMsgId(msgId);
            recordInfo.setToken(token);
            recordInfo.setDeviceSN(deviceSN);
            recordInfo.setProtocol(protocl);
            recordInfo.setMsgLevel(msgLevel);
            recordInfo.setProtodesc(protodesc);
            recordInfo.setAddTime(timestamp);
            recordInfo.setKey(record.getKey());
            recordInfo.setValue(value);
            recordInfo.setDesc(record.getDesc());

            createMessageRecord(recordInfo);

        }


    }

    /**
     * 创建消息记录
     *
     * @param msgId    消息id
     * @param token    网关
     * @param deviceSN 设备sn
     * @param protocl  解析的协议
     * @param parms    参数
     */
    @Async
    public void createMessageRecord4Tttech(Integer msgId, String token, String deviceSN, String protocl, Map<String, String> parms) {

        Integer timestamp = UnixTimeHelper.getUnixTimeStamp();
        for (String key : parms.keySet()) {


            String value = parms.get(key);

            if (StringHelper.isNullOrWhiteSpace(value)) {
                continue;
            }

            MessageRecordInfo recordInfo = new MessageRecordInfo();

            recordInfo.setMsgId(msgId);
            recordInfo.setToken(token);
            recordInfo.setDeviceSN(deviceSN);
            recordInfo.setProtocol(protocl);
            recordInfo.setAddTime(timestamp);
            recordInfo.setKey(key);
            recordInfo.setValue(value);

            createMessageRecord(recordInfo);

        }
    }

    /**
     * 更新一条消息解析记录数据
     *
     * @param messagerecordInfo 消息解析记录模型
     **/
    public MessageRecordInfo updateMessageRecord(MessageRecordInfo messagerecordInfo) {
        try {
            return messageRecords.updateMessageRecord(messagerecordInfo);
        } catch (Exception ex) {
            logs.write(ex, "更新一条消息解析记录数据异常");
        }

        return null;
    }

    /**
     * 删除一条消息解析记录数据
     *
     * @param recordId 消息解析记录模型
     **/
    public void deleteMessageRecordByRecordId(int recordId) {
        try {
            messageRecords.deleteMessageRecordByRecordId(recordId);
        } catch (Exception ex) {
            logs.write(ex, "删除一条消息解析记录数据异常");
        }
    }

    /**
     * 批量删除一批消息解析记录数据
     **/
    public void deleteMessageRecordByRecordIdList(String recordIdList) {
        try {
            messageRecords.deleteMessageRecordByRecordIdList(recordIdList);
        } catch (Exception ex) {
            logs.write(ex, "批量删除一批消息解析记录数据异常");
        }
    }

    /**
     * 获取一条消息解析记录数据
     *
     * @param recordId 消息解析记录模型
     **/
    public MessageRecordInfo getMessageRecordByRecordId(int recordId) {
        try {
            return messageRecords.getMessageRecordByRecordId(recordId);
        } catch (Exception ex) {
            logs.write(ex, "获取一条消息解析记录数据");
        }

        return null;
    }


    /**
     * 获得消息解析记录数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回MessageRecordInfo
     **/
    public List<MessageRecordInfo> getMessageRecordList(Specification<MessageRecordInfo> condition, Sort sort) {

        List<MessageRecordInfo> messageRecordList = new ArrayList<MessageRecordInfo>();

        try {
            messageRecordList = messageRecords.getMessageRecordList(condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得消息解析记录数据列表异常");
        }

        return messageRecordList;
    }


    /**
     * 获得消息解析记录数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回MessageRecordInfo
     **/
    public Page<MessageRecordInfo> getMessageRecordList(Integer pageSize, Integer pageNumber, Specification<MessageRecordInfo> condition, Sort sort) {

        Page<MessageRecordInfo> messageRecordList = null;

        try {
            messageRecordList = messageRecords.getMessageRecordList(pageSize, pageNumber, condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得消息解析记录数据列表异常");
        }

        return messageRecordList;
    }


    /**
     * 获取消息的解析列表
     *
     * @param msgId 消息id
     * @return
     */
    public List<MessageRecordInfo> getMessageRecordList(Integer msgId) {

        List<MessageRecordInfo> recordInfoList = new ArrayList<>();

        try {

            recordInfoList = messageRecords.getMessageRecordList(msgId);

        } catch (Exception ex) {

        }
        return recordInfoList;
    }

    /**
     * 获取参数的值
     *
     * @param recordInfoList 参数列表
     * @param key            关键词
     * @param defaultValue   默认值
     * @return
     */
    public String getParamValue(List<MessageRecordInfo> recordInfoList, String key, String defaultValue) {
        String returnStr = defaultValue;

        try {

            for (MessageRecordInfo info : recordInfoList) {

                if (info.getKey().toLowerCase().equals(key.toLowerCase())) {
                    returnStr = info.getValue();
                    break;
                }
            }

        } catch (Exception ex) {
            logs.write(ex, "获取参数的值");
        }

        return returnStr;
    }

    /**
     * 获取解析列表的条件
     *
     * @param token
     * @param lastTime
     * @param endTime
     * @return
     */
    public Specification<MessageRecordInfo> getMessageRecordListCondition(String token, Integer lastTime, Integer endTime) {

        return (Specification<MessageRecordInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();

            list.add(cb.equal(root.get("token").as(String.class), token));

            if (lastTime >= 1 && endTime >= 1) {
                list.add(cb.between(root.get("addTime").as(Integer.class), lastTime, endTime));
            }
            Predicate[] p = new Predicate[list.size()];

            query.where(cb.and(list.toArray(p)));

            return query.getGroupRestriction();
        };
    }


    //endregion


    //region  心跳消息方法

    /**
     * 获得心跳消息数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getHeartMessageCount(Specification<HeartMessageInfo> condition) {

        try {
            return heartMessages.getHeartMessageCount(condition);
        } catch (Exception ex) {
            logs.write(ex, "获得心跳消息数量失败");
        }
        return 0;
    }

    /**
     * 创建一条心跳消息数据
     *
     * @param heartmessageInfo 心跳消息模型
     * @return 返回创建信息
     **/
    @Async
    public void createHeartMessage(HeartMessageInfo heartmessageInfo) {
        try {
            heartMessages.createHeartMessage(heartmessageInfo);
        } catch (Exception ex) {
            logs.write(ex, "创建一条心跳消息数据失败");
        }
    }

    /**
     * 创建一条心跳消息数据
     *
     * @param s 心跳消息模型
     * @return 返回创建信息
     **/
    public void createHeartMessage(String s) {

        try {
            createHeartMessage(HeartMessageInfo.of(s));
        } catch (Exception ex) {
            logs.write(ex, "创建一条心跳包");
        }

    }

    /**
     * 更新一条心跳消息数据
     *
     * @param heartmessageInfo 心跳消息模型
     **/
    public HeartMessageInfo updateHeartMessage(HeartMessageInfo heartmessageInfo) {
        try {
            return heartMessages.updateHeartMessage(heartmessageInfo);
        } catch (Exception ex) {
            logs.write(ex, "更新一条心跳消息数据异常");
        }

        return null;
    }

    /**
     * 删除一条心跳消息数据
     *
     * @param msgId 心跳消息模型
     **/
    public void deleteHeartMessageByMsgId(int msgId) {
        try {
            heartMessages.deleteHeartMessageByMsgId(msgId);
        } catch (Exception ex) {
            logs.write(ex, "删除一条心跳消息数据异常");
        }
    }

    /**
     * 批量删除一批心跳消息数据
     **/
    public void deleteHeartMessageByMsgIdList(String msgIdList) {
        try {
            heartMessages.deleteHeartMessageByMsgIdList(msgIdList);
        } catch (Exception ex) {
            logs.write(ex, "批量删除一批心跳消息数据异常");
        }
    }

    /**
     * 获取一条心跳消息数据
     *
     * @param msgId 心跳消息模型
     **/
    public HeartMessageInfo getHeartMessageByMsgId(int msgId) {
        try {
            return heartMessages.getHeartMessageByMsgId(msgId);
        } catch (Exception ex) {
            logs.write(ex, "获取一条心跳消息数据");
        }

        return null;
    }


    /**
     * 获得心跳消息数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回HeartMessageInfo
     **/
    public List<HeartMessageInfo> getLastHeartMessageList(Specification<HeartMessageInfo> condition, Sort sort) {

        List<HeartMessageInfo> heartMessageList = new ArrayList<HeartMessageInfo>();

        try {
            heartMessageList = heartMessages.getHeartMessageList(condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得心跳消息数据列表异常");
        }

        return heartMessageList;
    }


    /**
     * 获得心跳消息数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回HeartMessageInfo
     **/
    public Page<HeartMessageInfo> getLastHeartMessageList(Integer pageSize, Integer
            pageNumber, Specification<HeartMessageInfo> condition, Sort sort) {

        Page<HeartMessageInfo> heartMessageList = null;

        try {
            heartMessageList = heartMessages.getHeartMessageList(pageSize, pageNumber, condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得心跳消息数据列表异常");
        }

        return heartMessageList;
    }

    /**
     * 获取心跳包列表
     *
     * @param msgId   消息Id
     * @param token   token
     * @param keyword 关键词
     * @return
     */
    public Specification<HeartMessageInfo> getHeartMessageListCondition(Integer msgId, String token, String keyword) {

        return getHeartMessageListCondition(msgId, token, keyword, "");
    }


    /**
     * 获取心跳包列表条件
     *
     * @param msgId   消息Id
     * @param token   token
     * @param keyword 关键词
     * @param uid     uid
     * @return
     */
    public Specification<HeartMessageInfo> getHeartMessageListCondition(Integer msgId, String token, String
            keyword, String uid) {
        return (Specification<HeartMessageInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();

            if (msgId >= 1) {

                list.add(cb.equal(root.get("msgId").as(Integer.class), msgId));
            }

            if (StringHelper.isNotNullOrWhiteSpace(token)) {
                list.add(cb.equal(root.get("token").as(String.class), token));
            }

            if (StringHelper.isNotNullOrWhiteSpace(keyword)) {
                list.add(cb.like(root.get("content").as(String.class), "%" + keyword + "%"));
            }

            if (StringHelper.isNotNullOrWhiteSpace(uid)) {
                list.add(cb.equal(root.get("uid").as(String.class), uid));
            }


            Predicate[] p = new Predicate[list.size()];

            query.where(list.toArray(p));

            return query.getGroupRestriction();
        };
    }


    /**
     * 获取消息列表的条件
     *
     * @param deviceSN 设备编号
     * @return
     */
    public Specification<MessageInfo> getMessageListCondition(String deviceSN) {

        return (Specification<MessageInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();


            list.add(cb.equal(root.get("deviceSN").as(String.class), deviceSN));

            Predicate[] p = new Predicate[list.size()];

            query.where(cb.and(list.toArray(p)));

            return query.getGroupRestriction();
        };
    }

    /**
     * 设备编号
     *
     * @param deviceList
     * @param startTime
     * @param startMsgId
     * @return
     */
    public Specification<MessageInfo> getMessageListCondition(List<String> deviceList, Integer startTime, Integer startMsgId) {

        return (Specification<MessageInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();

            CriteriaBuilder.In<String> in = cb.in(root.get("deviceSN"));

            for (String deviceSN : deviceList) {
                in.value(deviceSN);
            }

            query.where(cb.and());


            if (startMsgId >= 1) {
                list.add(cb.greaterThan(root.get("msgId").as(Integer.class), startMsgId));
            }

            if (startTime >= 1) {
                list.add(cb.greaterThan(root.get("timestamp").as(Integer.class), startTime));
            }


            Predicate[] p = new Predicate[list.size()];

            query.where(cb.and(list.toArray(p)), in);

            return query.getGroupRestriction();
        };
    }

    public Specification<HeartMessageInfo> getHeartMessageListCondition(String token) {

        return (Specification<HeartMessageInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();


            if (StringHelper.isNotNullOrWhiteSpace(token)) {
                list.add(cb.equal(root.get("token").as(String.class), token));
            }

            Predicate[] p = new Predicate[list.size()];

            query.where(cb.and(list.toArray(p)));

            return query.getGroupRestriction();
        };
    }


    /**
     * 查询图表数据记录条件
     *
     * @param deviceSN
     * @param protocol
     * @param key2
     * @return
     */
    public Specification<MessageRecordInfo> getChartRecordCondition(String deviceSN, String protocol, String key2) {
        return (Specification<MessageRecordInfo>) (root, query, cb) -> {
            List<Predicate> list = new ArrayList<>();
            if (!Strings.isNullOrEmpty(deviceSN)) {
                list.add(cb.equal(root.get("deviceSN").as(String.class), deviceSN));
            }
            if (!Strings.isNullOrEmpty(protocol)) {
                list.add(cb.equal(root.get("protocol").as(String.class), protocol));
            }
            if (!Strings.isNullOrEmpty(key2)) {
                list.add(cb.equal(root.get("key").as(String.class), key2));
            }
            Predicate[] p = new Predicate[list.size()];
            query.where(cb.and(list.toArray(p)));
            return query.getGroupRestriction();
        };
    }

    /**
     * 更新消息图片
     *
     * @param msgId  消息id
     * @param litpic 图片
     */
    public void updateMessageLitpic(Integer msgId, String litpic) {

        try {
            if (msgId <= 0) {
                throw new IllegalArgumentException("消息Id异常");
            }

            if (StringHelper.isNullOrWhiteSpace(litpic)) {
                throw new IllegalArgumentException("图片参数异常");
            }

            messages.updateMessageLitpic(msgId, litpic);

        } catch (Exception ex) {
            logs.write(ex, "更新图片");
        }

    }

    /**
     * 获取网关的消息列表
     *
     * @param pageSize 条数
     * @param token    网关
     * @return
     */
    public List<HeartMessageInfo> getLastHeartMessageList(Integer pageSize, String token) {

        List<HeartMessageInfo> messageInfoList = new ArrayList<>();

        try {

            if (StringHelper.isNullOrWhiteSpace(token)) {
                throw new IllegalArgumentException("token参数异常");
            }

            Specification<HeartMessageInfo> condition = (Specification<HeartMessageInfo>) (root, query, cb) -> {

                List<Predicate> list = new ArrayList<>();

                list.add(cb.equal(root.get("token").as(String.class), token));

                Predicate[] p = new Predicate[list.size()];

                query.where(cb.and(list.toArray(p)));

                return query.getGroupRestriction();

            };

            Sort sort = new Sort(Sort.Direction.DESC, "msgId");

            messageInfoList = getLastHeartMessageList(pageSize, 1, condition, sort).getContent();


        } catch (Exception ex) {
            logs.write(ex, "");
        }

        return messageInfoList;
    }


    /**
     * 获取设备的最新消息
     *
     * @param pageSize 条数
     * @param deviceSN 编号
     * @return 设备的最新消息
     */
    public List<MessageInfo> getLastMessageList(Integer pageSize, String deviceSN) {

        List<MessageInfo> messageInfoList = new ArrayList<>();

        try {

            if (StringHelper.isNullOrWhiteSpace(deviceSN)) {
                throw new IllegalArgumentException("token参数异常");
            }

            Specification<MessageInfo> condition = (Specification<MessageInfo>) (root, query, cb) -> {

                List<Predicate> list = new ArrayList<>();

                list.add(cb.equal(root.get("deviceSN").as(String.class), deviceSN));

                Predicate[] p = new Predicate[list.size()];

                query.where(cb.and(list.toArray(p)));

                return query.getGroupRestriction();

            };

            Sort sort = new Sort(Sort.Direction.DESC, "msgId");

            messageInfoList = getMessageList(pageSize, 1, condition, sort).getContent();


        } catch (Exception ex) {
            logs.write(ex, "");
        }

        return messageInfoList;
    }

    /**
     * 获取最后一包心跳包时间
     *
     * @param token token
     * @return 获取最后一包心跳包时间
     */
    public Integer getLastHeartMessageTimeByToken(String token) {

        try {

            if (StringHelper.isNullOrWhiteSpace(token)) {
                throw new IllegalArgumentException("token参数异常");
            }

            Integer lastHeartMessageTime = messages.getLastHeartMessageTimeByToken(token);

            if (lastHeartMessageTime == null || lastHeartMessageTime <= 0) {
                throw new IllegalArgumentException("回传参数异常");
            }

            return lastHeartMessageTime;

        } catch (Exception ex) {
            logs.write(ex, "获取最后一包心跳包时间");
        }

        return 0;
    }

    /**
     * 获取最后一包数据包时间
     *
     * @param deviceSN 设备编号
     * @return 最后一包数据包时间
     */
    public Integer getLastDataMessageTimeByDeviceSN(String deviceSN) {

        try {

            if (StringHelper.isNullOrWhiteSpace(deviceSN)) {
                throw new IllegalArgumentException("deviceSN参数异常");
            }

            Integer lastHeartMessageTime = messages.getLastDataMessageTimeByDeviceSN(deviceSN);

            if (lastHeartMessageTime == null || lastHeartMessageTime <= 0) {
                throw new IllegalArgumentException("回传参数异常");
            }

            return lastHeartMessageTime;

        } catch (Exception ex) {
            logs.write(ex, "获取最后一包数据包时间");
        }

        return 0;
    }

    /**
     * 根据关键字和设备token 时间区间查询
     *
     * @param token     token
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @param keys      关键词
     * @return 返回条件
     */
    public Specification<MessageRecordInfo> getMessageRecordListConditionByKeys(String token, Integer startTime, Integer endTime, List<String> keys) {
        return (Specification<MessageRecordInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();
            //CriteriaBuilder.In<String> in = cb.in(root.get("key"));
            //
            //for (String key : keys) {
            //    in.value(key);
            //}

            list.add(cb.equal(root.get("token").as(String.class), token));
            //list.add(in);
            if (startTime > 1 && endTime > 1) {
                list.add(cb.between(root.get("addTime").as(Integer.class), startTime, endTime));
            }
            Predicate[] p = new Predicate[list.size()];
            query.where(cb.and(list.toArray(p)));

            return query.getGroupRestriction();
        }
                ;
    }


    /**
     * 更新设备最后一包图片
     *
     * @param deviceSN 设备编号
     * @param litpic   图片地址
     */
    public void updateLastMessageLitpic(String deviceSN, String litpic, Integer timestamp, String... protocols) {


        try {

            if (protocols == null || protocols.length <= 0) {
                protocols = new String[]{"0XB0", "0XB1", "00A1"};
            }

            MessageInfo lastMessageInfo = getLastMessageByDeviceSNAndProtocol(deviceSN, timestamp, protocols);

            if (lastMessageInfo == null) {
                return;
            }

            lastMessageInfo.setLitpic(litpic);

            lastMessageInfo = messages.updateMessage(lastMessageInfo);

            if (lastMessageInfo == null || lastMessageInfo.getMsgId() <= 0) {
                logs.write("更新一条消息数据异常");
                return;
            }

            //outputImage(lastMessageInfo.getMsgId());

        } catch (Exception ex) {
            logs.write(ex, "更新一条消息数据异常");
        }


    }

    /**
     * 更新设备最后一包图片
     *
     * @param deviceSN 设备编号
     * @param waveData 火焰波形
     */
    public void updateLastMessageWaveData(String deviceSN, String waveData, String protocol) {
        try {

            MessageInfo lastMessageInfo = getLastMessageByDeviceSNAndProtocol(deviceSN, protocol);

            if (lastMessageInfo == null) {
                return;
            }

            ProtocolRecord record = new ProtocolRecord("waveData", waveData, "火焰波形");

            createMessageRecord(lastMessageInfo.getMsgId(), lastMessageInfo.getToken(), deviceSN, "0021", MsgLevel.Data.getLevel(), "火焰波形", Lists.newArrayList(record));


            lastMessageInfo = messages.updateMessage(lastMessageInfo);

            if (lastMessageInfo == null || lastMessageInfo.getMsgId() <= 0) {
                logs.write("更新一条消息数据异常");
            }

        } catch (Exception ex) {
            logs.write(ex, "更新一条消息数据异常");
        }
    }

    /**
     * 需删除的操作记录
     *
     * @param type 类型
     * @param time 时间
     * @return 条件
     */
    public Specification<HeartMessageInfo> getByTypeAndTime(Integer type, Integer time) {

        return (Specification<HeartMessageInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();

            list.add(cb.equal(root.get("type").as(Integer.class), type));

            if (time >= 1) {
                list.add(cb.lt(root.get("timestamp"), time - 90 * 24 * 60 * 60));
            }

            Predicate[] p = new Predicate[list.size()];

            query.where(cb.and(list.toArray(p)));

            return query.getGroupRestriction();

        };
    }

    /**
     * 获取网关状态改变的列表
     *
     * @param token token
     * @param start 开始时间
     * @param stop  结束时间
     * @return 条件
     */
    public Specification<HeartMessageInfo> getDeviceChangeListByToken(String token, Integer start, Integer stop, Integer content) {

        return (Specification<HeartMessageInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();

            if (StringHelper.isNotNullOrWhiteSpace(token)) {

                list.add(cb.like(root.get("token").as(String.class), "%" + token + "%"));
            }

            if (start != null && start != 0) {

                list.add(cb.ge(root.get("timestamp").as(Integer.class), start));
            }

            if (stop != null && stop != 0) {

                list.add(cb.le(root.get("timestamp").as(Integer.class), stop));
            }

            if (content != null && content != -1) {

                list.add(cb.equal(root.get("content").as(String.class), String.valueOf(content)));
            }

            list.add(cb.equal(root.get("type").as(Integer.class), 4));

            Predicate[] p = new Predicate[list.size()];

            query.where(cb.and(list.toArray(p)));

            return query.getGroupRestriction();

        };
    }


    //endregion

    //region  图片消息方法

    /**
     * 获得图片消息数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getMessageImageCount(Specification<MessageImageInfo> condition) {

        try {
            return messageImages.getMessageImageCount(condition);
        } catch (Exception ex) {
            logs.write(ex, "获得图片消息数量失败");
        }
        return 0;
    }

    /**
     * 创建一条图片消息数据
     *
     * @param messageimageInfo 图片消息模型
     * @return 返回创建信息
     **/
    public MessageImageInfo createMessageImage(MessageImageInfo messageimageInfo) {
        try {
            return messageImages.createMessageImage(messageimageInfo);
        } catch (Exception ex) {
            logs.write(ex, "创建一条图片消息数据失败");
        }
        return null;
    }

    /**
     * 更新一条图片消息数据
     *
     * @param messageimageInfo 图片消息模型
     **/
    public MessageImageInfo updateMessageImage(MessageImageInfo messageimageInfo) {
        try {
            return messageImages.updateMessageImage(messageimageInfo);
        } catch (Exception ex) {
            logs.write(ex, "更新一条图片消息数据异常");
        }

        return null;
    }

    /**
     * 删除一条图片消息数据
     *
     * @param imgId 图片消息模型
     **/
    public void deleteMessageImageByImgId(Integer imgId) {
        try {
            messageImages.deleteMessageImageByImgId(imgId);
        } catch (Exception ex) {
            logs.write(ex, "删除一条图片消息数据异常");
        }
    }

    /**
     * 批量删除一批图片消息数据
     **/
    public void deleteMessageImageByImgIdList(String imgIdList) {
        try {
            messageImages.deleteMessageImageByImgIdList(imgIdList);
        } catch (Exception ex) {
            logs.write(ex, "批量删除一批图片消息数据异常");
        }
    }

    /**
     * 获取一条图片消息数据
     *
     * @param imgId 图片消息模型
     **/
    public MessageImageInfo getMessageImageByImgId(Integer imgId) {

        try {
            return messageImages.getMessageImageByImgId(imgId);
        } catch (Exception ex) {
            logs.write(ex, "获取一条图片消息数据");
        }

        return null;
    }


    /**
     * 获得图片消息数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回MessageImageInfo
     **/
    public List<MessageImageInfo> getMessageImageList(Specification<MessageImageInfo> condition, Sort sort) {

        List<MessageImageInfo> messageImageList = new ArrayList<MessageImageInfo>();

        try {
            messageImageList = messageImages.getMessageImageList(condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得图片消息数据列表异常");
        }

        return messageImageList;
    }


    /**
     * 获得图片消息数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回MessageImageInfo
     **/
    public Page<MessageImageInfo> getMessageImageList(Integer pageSize, Integer pageNumber, Specification<MessageImageInfo> condition, Sort sort) {

        Page<MessageImageInfo> messageImageList = null;

        try {
            messageImageList = messageImages.getMessageImageList(pageSize, pageNumber, condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得图片消息数据列表异常");
        }

        return messageImageList;
    }

    /**
     * 获取图片分组
     *
     * @param type      分类
     * @param deviceSN  设备编号
     * @param shortTime 设备分组
     * @return 返回同组数据
     */
    public List<MessageImageInfo> getMessageImageList(Integer type, String deviceSN, String shortTime) {

        List<MessageImageInfo> imageInfoList = new ArrayList<>();

        try {

            Specification<MessageImageInfo> condition = (Specification<MessageImageInfo>) (root, query, cb) -> {

                List<Predicate> list = new ArrayList<>();

                list.add(cb.equal(root.get("type").as(Integer.class), type));

                list.add(cb.equal(root.get("state").as(Integer.class), 0));

                list.add(cb.equal(root.get("deviceSN").as(String.class), deviceSN));

                list.add(cb.equal(root.get("shortTime").as(String.class), shortTime));

                Predicate[] p = new Predicate[list.size()];

                query.where(cb.and(list.toArray(p)));

                return query.getGroupRestriction();
            };

            Sort sort = new Sort(Sort.Direction.ASC, "sort");

            imageInfoList = getMessageImageList(condition, sort);

        } catch (Exception ex) {
            logs.write(ex, "获取图片分组");
        }
        return imageInfoList;
    }

    /**
     * 获取过期图片分组
     *
     * @param type 类型
     */
    public List<MessageImageInfo> getOverdueMessageImageList(Integer type, Integer timeUnit, Integer value) {

        List<MessageImageInfo> imageInfoList = new ArrayList<>();

        try {

            Integer timestamp = UnixTimeHelper.getBeforeTime(timeUnit, value);

            Specification<MessageImageInfo> condition = (Specification<MessageImageInfo>) (root, query, cb) -> {

                List<Predicate> list = new ArrayList<>();

                list.add(cb.equal(root.get("type").as(Integer.class), type));

                list.add(cb.equal(root.get("state").as(Integer.class), 0));

                list.add(cb.lessThan(root.get("timestamp").as(Integer.class), timestamp));

                Predicate[] p = new Predicate[list.size()];

                query.where(cb.and(list.toArray(p)));

                return query.getGroupRestriction();

            };


            Sort sort = new Sort(Sort.Direction.ASC, "sort");

            imageInfoList = getMessageImageList(condition, sort);

        } catch (Exception ex) {
            logs.write(ex, "获取过期图片分组");
        }

        return imageInfoList;
    }

    /**
     * 消息图片入库
     *
     * @param deviceSN  设备编号
     * @param shortTime 拍照时间
     * @param total     分包总数
     * @param sort      本包序号
     * @param imgData   图片二进制
     * @return 返回分组包
     */
    public List<MessageImageInfo> createMessageImage(Integer type, String deviceSN, String imgData, Integer sort, Integer total, String shortTime) {

        List<MessageImageInfo> list = new ArrayList<>();

        try {
            MessageImageInfo messageImageInfo = new MessageImageInfo(type, deviceSN, imgData, shortTime, sort, total);

            messageImageInfo = createMessageImage(messageImageInfo);

            if (messageImageInfo == null || messageImageInfo.getImgId() <= 0) {
                throw new IllegalArgumentException("图片入库失败");
            }

            list = getMessageImageList(type, deviceSN, shortTime);

        } catch (Exception ex) {
            logs.write(ex, "消息图片入库");
        }

        return list;
    }


    /**
     * 更新图片库状态
     *
     * @param state     类型
     * @param type      类型
     * @param deviceSN  设备编号
     * @param shortTime 分组时间
     */
    public void updateMessageImageSate(Integer state, Integer type, String deviceSN, String shortTime) {
        try {
            messageImages.updateMessageImageSate(state, type, deviceSN, shortTime);
        } catch (Exception ex) {
            logs.write(ex, "更新图片库状态");
        }

    }

    //endregion

}
