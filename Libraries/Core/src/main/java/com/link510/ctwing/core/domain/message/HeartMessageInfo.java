package com.link510.ctwing.core.domain.message;

import com.alibaba.fastjson.JSON;
import com.link510.ctwing.core.exption.message.ErrorMessageException;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 心跳包
 *
 * @author cqnews
 */

@ToString
@Entity
@DynamicUpdate
@Table(name = "w_messages_heart")
public class HeartMessageInfo extends SupperMessageInfo implements Serializable {
    private static final long serialVersionUID = 1318620118921110774L;

    /**
     * 消息的组装
     *
     * @param content 内容
     * @return
     */
    public static HeartMessageInfo of(String content) throws ErrorMessageException {
        try {

            return JSON.parseObject(content, HeartMessageInfo.class);

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ErrorMessageException(ex, "消息组装失败");
        }
    }


    /**
     * 消息的组装
     *
     * @param messageInfo 内容
     * @return
     */
    public static HeartMessageInfo of(MessageInfo messageInfo, Integer type) throws ErrorMessageException {

        try {

            HeartMessageInfo heartMessageInfo = JSON.parseObject(JSON.toJSONString(messageInfo), HeartMessageInfo.class);

            heartMessageInfo.setType(type);

            return heartMessageInfo;

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ErrorMessageException(ex, "消息组装失败");
        }

    }


    /**
     * 消息的组装
     *
     * @param messageInfo 内容
     * @return
     */
    public static HeartMessageInfo of(MessageInfo messageInfo) throws ErrorMessageException {
        try {

            return JSON.parseObject(JSON.toJSONString(messageInfo), HeartMessageInfo.class);

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ErrorMessageException(ex, "消息组装失败");
        }
    }

    public HeartMessageInfo() {
    }

    public HeartMessageInfo(String uid, Integer agentId, String unitCode, String deviceSN, Integer productId, Integer productType, String protocol, Integer msgLevel, String protodesc, String token, String channelId, String ip, Integer port, Integer pluginId, Integer type, Integer state, String message, String content, String deviceName, String deviceCode, String topic, Integer timestamp, Integer lose) {
        super(uid, agentId, unitCode, deviceSN, productId, productType, protocol, msgLevel, protodesc, token, channelId, ip, port, pluginId, type, state, message, content, deviceName, deviceCode, topic, timestamp, lose);
    }
}
