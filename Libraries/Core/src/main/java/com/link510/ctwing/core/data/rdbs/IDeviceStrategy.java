package com.link510.ctwing.core.data.rdbs;


import com.link510.ctwing.core.domain.device.DeviceInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.io.IOException;
import java.util.List;


/**
 * 设备策略
 */

public interface IDeviceStrategy {


    //region 设备

    /**
     * 获得设备数量
     *
     * @param condition 条件
     * @return 返回数量
     **/

    long getDeviceCount(Specification<DeviceInfo> condition) throws IOException;


    /**
     * 创建一条设备数据
     *
     * @param deviceInfo 设备模型
     * @return 返回创建信息
     **/

    DeviceInfo createDevice(DeviceInfo deviceInfo) throws IOException;

    /**
     * 校验主要的数据是否存在
     *
     * @param productId 产品id
     * @param deviceSN  设备编号
     */
    boolean isDeviceExists(Integer productId, String deviceSN) throws IOException;

    /**
     * 更新一条设备数据
     *
     * @param deviceInfo 设备模型
     **/

    DeviceInfo updateDevice(DeviceInfo deviceInfo) throws IOException;

    /**
     * 删除一条设备数据
     *
     * @param id 设备模型
     **/

    boolean deleteDeviceByDeviceId(int id) throws IOException;

    /**
     * 设备恢复
     *
     * @param deviceId 设备Id
     * @return
     */
    boolean recoverDeviceByDeviceId(Integer deviceId) throws IOException;

    /**
     * 批量删除一批设备数据
     **/

    void deleteDeviceByIdList(String idList) throws IOException;


    /**
     * 获得设备一条记录
     *
     * @param deviceId deviceId
     * @return 返回一条DeviceInfo
     **/
    DeviceInfo getDeviceById(int deviceId) throws IOException;

    /**
     * 通过设备号获取设备信息
     *
     * @param deviceSN
     * @return
     */
    DeviceInfo getDeviceByDeviceSN(String deviceSN) throws IOException;

    /**
     * 通过产品id和设备编号获取设备信息
     *
     * @param productId 产品id
     * @param deviceSN  产品编号
     * @return 返回设备信息
     */
    DeviceInfo getDeviceByProductIdAndDeviceSN(Integer productId, String deviceSN) throws IOException;

    /**
     * 获得设备数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回DeviceInfo
     **/
    List<DeviceInfo> getDeviceList(Specification<DeviceInfo> condition, Sort sort) throws IOException;


    /**
     * 获得设备数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回DeviceInfo
     **/
    Page<DeviceInfo> getDeviceList(Integer pageSize, Integer pageNumber, Specification<DeviceInfo> condition, Sort sort) throws IOException;


    /**
     * 查询NB设备信息
     *
     * @param token
     * @return
     * @throws IOException
     */
    DeviceInfo getNbDeviceByToken(String token) throws IOException;

    /**
     * 查询NB设备信息
     *
     * @param imsi
     * @return
     * @throws IOException
     */
    DeviceInfo getDeviceByImsi(String imsi) throws IOException;

    /**
     * 物理删除设备
     *
     * @param deviceId
     * @throws IOException
     */
    void deletePhysicsDeviceByDeviceId(Integer deviceId) throws IOException;

    //endregion 设备结束


}


