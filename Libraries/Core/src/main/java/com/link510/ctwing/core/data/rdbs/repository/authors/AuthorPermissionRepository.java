
package com.link510.ctwing.core.data.rdbs.repository.authors;

import com.link510.ctwing.core.data.rdbs.repository.BaseRepository;
import com.link510.ctwing.core.domain.authors.AuthorPermissionInfo;

import java.util.List;

/**
 * @author cqnews
 */
public interface AuthorPermissionRepository extends BaseRepository<AuthorPermissionInfo, Integer> {

    List<AuthorPermissionInfo> findAllByRoleId(Integer roleId);
}