package com.link510.ctwing.core.enums.message;

public enum DeviceModel {

    /**
     * 正常模式
     */
    Normal(0, "正常模式"),

    /**
     * 正常模式
     */
    DeBug(1, "调试模型");


    /**
     * index
     */
    private Integer index;

    /**
     * 说明
     */
    private String name;

    DeviceModel(Integer index, String name) {
        this.index = index;
        this.name = name;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
