/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.link510.ctwing.core.data;


import com.link510.ctwing.core.data.rdbs.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by cqnews on 2017/4/10.
 */
@Service(value = "CWMData")
@Setter
@Getter
public class CWMData {

    @Autowired(required = false)
    private IUserStrategy iUserStrategy;

    @Autowired(required = false)
    private IAuthorStrategy iAuthorStrategy;

    @Autowired(required = false)
    private ILog2Strategy iLog2Strategy;

    @Autowired(required = false)
    private IBaseStrategy iBaseStrategy;

    @Autowired(required = false)
    private IMessageStrategy iMessageStrategy;

    @Autowired(required = false)
    private IDeviceStrategy iDeviceStrategy;


}
