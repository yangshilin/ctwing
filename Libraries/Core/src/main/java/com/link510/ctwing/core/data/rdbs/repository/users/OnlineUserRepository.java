package com.link510.ctwing.core.data.rdbs.repository.users;

import com.link510.ctwing.core.data.rdbs.repository.BaseRepository;
import com.link510.ctwing.core.domain.users.OnlineUserInfo;

public interface OnlineUserRepository extends BaseRepository<OnlineUserInfo, Integer> {
}
