
package com.link510.ctwing.core.data.rdbs.repository.messages;

import com.link510.ctwing.core.data.rdbs.repository.BaseRepository;
import com.link510.ctwing.core.domain.message.MessageImageInfo;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

public interface MessageImageRepository extends BaseRepository<MessageImageInfo, Integer> {

    /**
     * 更新图片库状态
     *
     * @param state     类型
     * @param type      类型
     * @param deviceSN  设备编号
     * @param shortTime 分组时间
     */
    @Transactional(rollbackFor = IOException.class)
    @Modifying
    @Query("update MessageImageInfo info set info.state = ?1 where info.type =?2 and info.deviceSN = ?3 and info.shortTime = ?4")
    void updateMessageImageSate(Integer state, Integer type, String deviceSN, String shortTime) throws IOException;
}