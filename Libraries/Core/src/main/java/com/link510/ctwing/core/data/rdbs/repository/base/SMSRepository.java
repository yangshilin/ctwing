package com.link510.ctwing.core.data.rdbs.repository.base;

import com.link510.ctwing.core.data.rdbs.repository.BaseRepository;
import com.link510.ctwing.core.domain.sms.SMSInfo;

public interface SMSRepository extends BaseRepository<SMSInfo, Integer> {
}
