package com.link510.ctwing.core.data.rdbs.repository.messages;

import com.link510.ctwing.core.data.rdbs.repository.BaseRepository;
import com.link510.ctwing.core.domain.message.MessageInfo;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MessageRepository extends BaseRepository<MessageInfo, Integer> {


    /**
     * 丢弃消息
     * @param msgId
     */
    /**
     * 让消息失效
     *
     * @param msgId 消息id
     */
    @Transactional
    @Modifying
    @Query(value = "update MessageInfo info set info.lose =1 where info.msgId = ?1")
    void lose(Integer msgId);

    /**
     * 通过tokem查询消息列表
     *
     * @param token
     * @return
     */
    List<MessageInfo> findByToken(String token);

//
//    @Query("select  info from MessageInfo info ")
//    Page<MessageInfo> findlist2(Pageable pageable);


//    /**
//     * 根据token
//     */
//    @Query(value = "select round((info.timestamp/86400),0) as day,count(info.msgId) w_messages info group by day", nativeQuery = true)
//    List<Object> findList3();


    /**
     * 统计条数
     *
     * @param msgLevel  消息类别
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return
     */
    @Query(value = "select count(info.msgId) from MessageInfo info where info.msgLevel = ?1 and info.timestamp >= ?2 and info.timestamp <= ?3")
    long count(Integer msgLevel, Integer startTime, Integer endTime);


    /**
     * 统计条数
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return
     */
    @Query(value = "select count(info.msgId) from MessageInfo info where info.timestamp >= ?1 and info.timestamp <= ?2")
    long count(Integer startTime, Integer endTime);

    /**
     * 统计警告设备消息
     *
     * @return
     */
    @Query(value = "select count(info.msgId) from MessageInfo info group by info.deviceSN")
    List<Long> countWarningAllMessage();


    /**
     * 获取最后一条数据
     *
     * @param deviceSN 设备编号
     * @param protocol 协议名称
     * @return
     */
    MessageInfo findFirstByDeviceSNAndProtocolOrderByMsgIdDesc(String deviceSN, String protocol);

    /**
     * 更新图片
     *
     * @param msgId  消息id
     * @param litpic 图片
     */
    @Query(value = "update MessageInfo info set info.litpic =?2 where info.msgId=?1")
    void updateMessageLitpic(Integer msgId, String litpic);

    /**
     * 获取最后一包数据包时间
     *
     * @param deviceSN 设备编号
     * @return
     */
    @Query(value = "select max(info.timestamp) from MessageInfo info where info.deviceSN = ?1 order by info.msgId desc")
    Integer getLastDataMessageTimeByDeviceSN(String deviceSN);
}
