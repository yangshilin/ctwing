package com.link510.ctwing.core.data.rdbs.repository.base;

import com.link510.ctwing.core.data.rdbs.repository.BaseRepository;
import com.link510.ctwing.core.domain.base.EventLogInfo;

public interface EventLogRepository extends BaseRepository<EventLogInfo, Integer> {



}
