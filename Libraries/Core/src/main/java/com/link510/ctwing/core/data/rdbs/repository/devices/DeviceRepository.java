
package com.link510.ctwing.core.data.rdbs.repository.devices;

import com.link510.ctwing.core.data.rdbs.repository.BaseRepository;
import com.link510.ctwing.core.domain.device.DeviceInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;



public interface DeviceRepository extends BaseRepository<DeviceInfo, Integer> {


    /**
     * 通过设备编号查询设备信息
     *
     * @param deviceSN 设备编号
     * @return
     */

    DeviceInfo findFirstByDeviceSN(String deviceSN);


    /**
     * 通过产品id和设备编号获取设备信息
     *
     * @param productId 产品id
     * @param deviceSN  产品编号
     * @return 返回设备信息
     */
    DeviceInfo findFirstByProductIdAndDeviceSN(Integer productId, String deviceSN);

    /**
     * 判断同一个产品下面的设备编号是否唯一
     */
    boolean existsByProductIdAndDeviceSN(Integer productId, String deviceSN);



    @Query("select info from DeviceInfo info where info.deviceSN=?1")
    DeviceInfo getDeviceByDeviceSN(String deviceSn);


    /**
     * 更新设备的状态
     *
     * @param warningLevel 设备等级
     * @param overdueTime  结束时间
     * @return
     */
    @Transactional
    @Modifying
    @Query("update DeviceInfo info set info.warningLevel = 0 where info.warningLevel = ?1 and info.warningTime <= ?2 ")
    Integer batchUpdateDeviceWarningLevel(Integer warningLevel, Integer overdueTime);


    @Transactional
    @Modifying
    @Query("update DeviceInfo info set info.isDelete = 1 where info.deviceId = ?1")
    Integer deleteDeviceByDeviceId(int deviceId);

    @Transactional
    @Modifying
    @Query("update DeviceInfo info set info.isDelete = 0 where info.deviceId = ?1")
    Integer recoverDeviceByDeviceId(int deviceId);

    /**
     * 批量修改设备模式
     *
     * @param productId   产品Id
     * @param unitCode    单位编号
     * @param deviceModel 设备模式
     */
    @Transactional
    @Modifying
    @Query("update DeviceInfo info set info.deviceModel = ?3 where info.productId = ?1 and  info.unitCode = ?2")
    Integer batchupdateDeviceModel(Integer productId, String unitCode, Integer deviceModel);

    /**
     * 批量修改设备模式
     *
     * @param unitCode    单位编号
     * @param deviceModel 设备模式
     */
    @Transactional
    @Modifying
    @Query("update DeviceInfo info set info.deviceModel = ?2 where info.unitCode = ?1")
    Integer batchupdateDeviceModel(String unitCode, Integer deviceModel);


    /**
     * 批量更新产品模型
     *
     * @param productId 产品Id
     * @param moduleId  产品模型
     */
    @Transactional
    @Modifying
    @Query("update DeviceInfo info set info.moduleId = ?2 where info.productId = ?1")
    Integer updateDeviceModuleId(Integer productId, Integer moduleId);

    /**
     * 查询NB设备信息
     *
     * @param token token
     * @return list
     */
    @Query("select info from DeviceInfo info where info.token=?1")
    List<DeviceInfo> getNbDeviceByToken(String token);

    /**
     * 查询NB设备信息
     *
     * @param token token
     * @return list
     */
    DeviceInfo findFirstByToken(String token);


    /**
     * 查询NB设备信息
     *
     * @param imsi imsi
     * @return list
     */
    DeviceInfo findFirstByImsi(String imsi);

    /**
     * 查询设备信息
     *
     * @param imsi
     * @return
     */
    @Query("select info from DeviceInfo  info where info.imsi=?1")
    List<DeviceInfo> getDeviceByImsi(String imsi);

    @Modifying
    @Transactional
    @Query("delete from DeviceInfo info where info.deviceId=?1")
    void deletePhysicsDeviceByDeviceId(Integer deviceId);

    @Query("select info from DeviceInfo info where info.moduleId=?1")
    List<DeviceInfo> getDeviceByModuleId(Integer moduleId);
}