package com.link510.ctwing.web.controller;

import com.link510.ctwing.services.Persons;
import com.link510.ctwing.services.Users;
import com.link510.ctwing.services.WechatMiniUtils;
import com.link510.ctwing.web.framework.controller.BaseWebController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "TestController")
public class TestController extends BaseWebController {

    @Autowired
    Persons persons;

    @Autowired
    Users users;

    @Autowired
    WechatMiniUtils wechatMiniUtils;


    /**
     * hellow
     *
     * @return
     */
    @RequestMapping(value = "/hello")
    public String hello() {


        return "我喜欢周婷";
    }


}
