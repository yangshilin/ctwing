/*
 *  *  Copyright  2019.
 *  *  用于JAVA项目开发
 *  *  重庆英卡电子有限公司 版权所有
 *  *  Copyright  2019.  510Link.com Iniot All rights reserved.
 */

package com.link510.ctwing.web.framework.controller;

import com.link510.ctwing.core.exption.CWMException;
import com.link510.ctwing.core.helper.WebHelper;
import com.link510.ctwing.web.framework.workcontext.NBIoTWorkContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(value = "nbiot")
public class BaseNBIoTController extends BaseController {

    private NBIoTWorkContext workContext;


    public BaseNBIoTController() {

    }

    @Override
    NBIoTWorkContext getWorkContext() {
        return workContext;
    }

    @ModelAttribute
    public void setInitialize(HttpServletResponse response, HttpServletRequest request) throws CWMException {


        try {

            this.response = response;
            this.request = request;
            this.workContext = new NBIoTWorkContext();
            this.session = request.getSession();


            /*
             * 获取当前URL
             */
            this.workContext.setUrl(WebHelper.getUrl(request));

            /*
             * 获取当前controller
             */
            this.workContext.setController(this.getClass().getName());

            /*
             * 获取当前action 暂时不能实现
             */
            this.workContext.setAction(WebHelper.getRawUrl(request));


            /*
             * 判断是否为ajax
             */
            if (WebHelper.isAjax(request)) {
                this.workContext.setHttpAjax(true);
            }

            /*
             * 获取IP
             */
            this.workContext.setIP(WebHelper.getIP(request));

            this.workContext.setUrlReferrer(WebHelper.getUrlReferrer(request));


        } catch (Exception ex) {

            logs.write(ex, "上午文处理报错");

            throw new CWMException("异常退出");
        }


    }


    @ModelAttribute
    public void inspectInitialize() { //System.out.println("inspectInitialize被执行了");
    }
}
