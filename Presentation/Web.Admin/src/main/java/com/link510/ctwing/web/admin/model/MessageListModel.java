package com.link510.ctwing.web.admin.model;

import com.link510.ctwing.core.domain.message.MessageInfo;
import com.link510.ctwing.core.model.PageModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MessageListModel {

    /**
     * 消息Id
     */
    private Integer msgId = -1;

    /**
     * token
     */
    private String token = "";


    /**
     * 设备编号
     */
    private String deviceSN = "";

    /**
     * keyword
     */
    private String keyword = "";

    /**
     * 消息列表
     */
    List<MessageInfo> messageInfoList;

    /**
     * 分页模型
     */
    PageModel pageModel;


}
