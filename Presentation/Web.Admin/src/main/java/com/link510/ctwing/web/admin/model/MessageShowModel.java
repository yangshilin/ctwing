package com.link510.ctwing.web.admin.model;

import com.link510.ctwing.core.domain.message.MessageInfo;
import com.link510.ctwing.core.domain.message.MessageRecordInfo;
import lombok.*;

import java.util.List;

/**
 * 消息展示模型
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MessageShowModel {


    /**
     * 消息模型
     */
    private MessageInfo messageInfo;

    /**
     * 解析列表
     */
    private List<MessageRecordInfo> recordInfoList;


}
