package com.link510.ctwing.web.nbiot.controller;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Strings;
import com.link510.ctwing.core.helper.ByteHelper;
import com.link510.ctwing.core.helper.HexHelper;
import com.link510.ctwing.web.framework.controller.BaseNBIoTController;
import com.link510.ctwing.web.nbiot.handle.HandleUtils;
import com.link510.ctwing.web.nbiot.model.ReceiveParamModel;
import jodd.util.Base64;
import org.apache.tomcat.util.buf.HexUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class BackCallController {

    @Autowired
    private HandleUtils handleUtils;

    /**
     * 设备的数据变化
     *
     * @param param
     * @return
     */
    @RequestMapping(value = "/nbiot/callback")
    public String callback(@RequestBody @Nullable ReceiveParamModel param) {

        String result = "";

        try {

            if (param == null) {
                throw new IllegalArgumentException("参数异常");
            }

            if (param.getPayload() == null) {
                throw new IllegalArgumentException("真实数据异常");
            }

            String appdata = param.getPayload().getAppdata();

            if (Strings.isNullOrEmpty(appdata)) {
                throw new IllegalArgumentException("数据异常");
            }

            String hexString = ByteHelper.base64ToHexString(appdata);

            handleUtils.hand(param.getDeviceId(), hexString);

            result = "ok";


        } catch (Exception ex) {
            result = "fail";
        }

        return result;
    }


    /**
     * 设备事件变化信息
     *
     * @param model 模型
     * @return
     */
    @RequestMapping(value = "callback/deviceEventChanged")
    public String device(@RequestBody @RequestParam(required = false) ReceiveParamModel model) {

        ResponseEntity<HttpStatus> result;
        try {
            result = new ResponseEntity<>(HttpStatus.OK);

            System.out.println("设备事件变化信息");

            if (model != null) {
                System.out.println(model.toString());
            }

            //System.out.println(model.toString());

        } catch (Exception ex) {
            result = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return "ok";
    }


    /**
     * 设备信息变化信息
     *
     * @return
     */
    @RequestMapping(value = "callback/deviceInfoChanged")
    public String deviceInfoChanged() {

        ResponseEntity<HttpStatus> result;
        try {
            result = new ResponseEntity<>(HttpStatus.OK);

            System.out.println("设备信息变化信息");

            //System.out.println(model.toString());

        } catch (Exception ex) {
            result = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return "ok";
    }

    /**
     * 设备上下线变化信息
     *
     * @return
     */
    @RequestMapping(value = "callback/deviceOnOffChanged")
    public String deviceOnOffChanged() {

        ResponseEntity<HttpStatus> result;
        try {
            result = new ResponseEntity<>(HttpStatus.OK);

            System.out.println("设备上下线变化信息");


            //System.out.println(model.toString());

        } catch (Exception ex) {
            result = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return "ok";
    }

    public static void main(String[] args) {
        String s = ByteHelper.base64ToHexString("Af0TADIAAAQAGVYAQVlLTkIC/gApAxlvHCUkDQAAAUw6AAAABQChB+MFFQsfBwIBD6AOJg1F");
        System.out.println(s);

    }
}
