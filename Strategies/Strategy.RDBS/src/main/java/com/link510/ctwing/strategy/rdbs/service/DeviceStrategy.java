package com.link510.ctwing.strategy.rdbs.service;

import com.link510.ctwing.core.data.rdbs.IDeviceStrategy;
import com.link510.ctwing.core.data.rdbs.repository.devices.DeviceRepository;
import com.link510.ctwing.core.domain.device.DeviceInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;


@Component(value = "DeviceStrategy")
public class DeviceStrategy extends RDBSService implements IDeviceStrategy {

    @Autowired
    private DeviceRepository deviceRepository;


    //region 设备

    /**
     * 获得设备数量
     *
     * @param condition 条件
     * @return 返回数量
     * @throws IOException
     **/
    @Override
    public long getDeviceCount(Specification<DeviceInfo> condition) throws IOException {

        return deviceRepository.count(condition);
    }


    /**
     * 创建一条设备数据
     *
     * @param deviceInfo 设备模型
     * @return 返回创建信息
     * @throws IOException
     **/
    @Override
    public DeviceInfo createDevice(DeviceInfo deviceInfo) throws IOException {

        return deviceRepository.save(deviceInfo);
    }

    /**
     * 校验主要的数据是否存在
     *
     * @param productId 产品id
     * @param deviceSN  设备编号
     * @return
     */
    @Override
    public boolean isDeviceExists(Integer productId, String deviceSN) throws IOException {

        return deviceRepository.existsByProductIdAndDeviceSN(productId, deviceSN);
    }

    /**
     * 更新一条设备数据
     *
     * @param deviceInfo 设备模型
     **/
    @Override
    public DeviceInfo updateDevice(DeviceInfo deviceInfo) throws IOException {

        if (deviceInfo.getDeviceId() >= 1) {
            return deviceRepository.save(deviceInfo);
        }

        return deviceInfo;

    }


    /**
     * 删除一条设备数据
     *
     * @param deviceId 设备模型
     **/
    @Override
    public boolean deleteDeviceByDeviceId(int deviceId) throws IOException {
        return deviceRepository.deleteDeviceByDeviceId(deviceId) >= 1;
        //deviceRepository.deleteById(id);
    }

    /**
     * 设备恢复
     *
     * @param deviceId 设备Id
     * @return
     */
    @Override
    public boolean recoverDeviceByDeviceId(Integer deviceId) throws IOException {
        return deviceRepository.recoverDeviceByDeviceId(deviceId) >= 1;
    }

    /**
     * 批量删除一批设备数据
     **/
    @Override
    public void deleteDeviceByIdList(String idlist) throws IOException {


    }

    /**
     * 获得设备一条记录
     *
     * @param deviceId deviceId
     * @return 返回一条DeviceInfo
     **/
    @Override
    public DeviceInfo getDeviceById(int deviceId) throws IOException {
        return deviceRepository.findById(deviceId).orElse(null);
    }

    /**
     * 通过设备号获取设备信息
     *
     * @param deviceSN  产品编号
     * @return
     */
    @Override
    public DeviceInfo getDeviceByDeviceSN(String deviceSN) throws IOException {
        return deviceRepository.findFirstByDeviceSN(deviceSN);
    }


    /**
     * 通过产品id和设备编号获取设备信息
     *
     * @param productId 产品id
     * @param deviceSN  产品编号
     * @return 返回设备信息
     */
    @Override
    public DeviceInfo getDeviceByProductIdAndDeviceSN(Integer productId, String deviceSN) throws IOException {

        return deviceRepository.findFirstByProductIdAndDeviceSN(productId, deviceSN);
    }

    /**
     * 获得设备数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回DeviceInfo
     **/
    @Override
    public List<DeviceInfo> getDeviceList(Specification<DeviceInfo> condition, Sort sort) throws IOException {

        if (sort == null) {
            sort = new Sort(Sort.Direction.DESC, "deviceId");
        }

        return deviceRepository.findAll(condition, sort);

    }


    /**
     * 获得设备数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回DeviceInfo
     **/
    @Override
    public Page<DeviceInfo> getDeviceList(Integer pageSize, Integer pageNumber, Specification<DeviceInfo> condition, Sort sort) throws IOException {

        if (pageNumber >= 1) {
            pageNumber--;
        }

        if (sort == null) {
            sort = new Sort(Sort.Direction.DESC, "deviceId");
        }

        Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);

        return deviceRepository.findAll(condition, pageable);


    }

    /**
     * 查询NB设备信息
     *
     * @param token
     * @return
     * @throws IOException
     */
    @Override
    public DeviceInfo getNbDeviceByToken(String token) throws IOException {
        return deviceRepository.findFirstByToken(token);
    }

    /**
     * 查询NB设备信息
     *
     * @param imsi
     * @return
     * @throws IOException
     */
    @Override
    public DeviceInfo getDeviceByImsi(String imsi) throws IOException {
        return deviceRepository.findFirstByImsi(imsi);
    }

    /**
     * 物理删除设备
     *
     * @param deviceId
     * @throws IOException
     */
    @Override
    public void deletePhysicsDeviceByDeviceId(Integer deviceId) throws IOException {
        deviceRepository.deletePhysicsDeviceByDeviceId(deviceId);
    }


//endregion


}







